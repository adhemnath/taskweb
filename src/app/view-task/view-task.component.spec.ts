import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ViewTaskComponent } from './view-task.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { SearchPipes } from 'src/app/Filter/search-pipes';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing'


describe('ViewTaskComponent', () => {
  let component: ViewTaskComponent;
  let fixture: ComponentFixture<ViewTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, FormsModule, RouterTestingModule, HttpClientModule, BrowserModule],
      providers: [ViewTaskComponent],
      declarations: [SearchPipes, ViewTaskComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTaskComponent);
    component = fixture.componentInstance;

    let getValue: any = {};
    getValue.className = "nav-link";
    getValue.innerHTML = "Add Task";

    spyOn(component, 'setaddTask').and.returnValue(getValue);
    spyOn(component, 'setviewTask').and.returnValue(getValue);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('getAllTaskse', () => {
    component.getTaskDetails();
  });
});
