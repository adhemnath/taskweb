import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms';
import { taskModel } from '../model/Task';
import {TaskServiceService} from 'src/app/task-service.service';

@Component({
  selector: 'app-view-task',
  templateUrl: './view-task.component.html',
  styleUrls: ['./view-task.component.css']
})
export class ViewTaskComponent implements OnInit {

  public viewTaskForm = new FormGroup({
  txtTask  : new FormControl(''),
  txtParentTask  : new FormControl(''),
  txtPriorityFrom  : new FormControl(''),
  txtPriorityTo  : new FormControl(''),
  dtStartDate  : new FormControl(''),
  dtEndDate  : new FormControl('')});
   
  constructor(private service:TaskServiceService) {};
  public eachTsk:taskModel;
  public taskCollection:taskModel[];
  public viewTask:any;
  public addTask:any;

  ngOnInit() {

    this.addTask = this.setaddTask();
    this.viewTask = this.setviewTask();
          
    this.viewTask.className = "nav-link active"
    this.addTask.className = "nav-link";  
   
    this.viewTask.innerText="View Task";
    this.addTask.innerText="Add Task";
   
    this.getTaskDetails();    
  }

  public setaddTask():any
  {
   return document.getElementById("addTask");
  }
  public setviewTask():any
  {
    return  document.getElementById("viewTask");
  }


  public getTaskDetails()
  {    
    this.service.getAllTasks().subscribe((TaskIDVar:taskModel[])=>this.taskCollection=TaskIDVar);
  }

  public endTask(taskId: number)
  {
    var tsk:taskModel;
    this.service.endTask(taskId).subscribe();
    tsk = this.taskCollection.find(x => x.Task_ID == taskId)
    if (tsk!=null)
    {
        tsk.isTaskEnd = true;
    }
  }

}
