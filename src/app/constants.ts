export class AppConstants {
    public baseUrl:string= "http://localhost:5000/WEBAPI/";
    //"http://localhost/TaskMgr.Service/";
    //"http://localhost:49494/TaskMgr/";    
    public getTasks:string="GetAllTasks";
    public getTaskByID:string="GetTasksByTaskId";
    public addTask:string="AddTask";
    public updateTask:string="UpdateTask";
    public endTask:string="EndTask";
}
