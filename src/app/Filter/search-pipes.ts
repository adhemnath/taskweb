import { Pipe, PipeTransform } from '@angular/core';
import { taskModel } from '../model/Task';
//import { taskModel } from 'src/app/Model/Task';

@Pipe({ name: 'SearchTask' ,pure: false})
export class SearchPipes implements PipeTransform {
    
    transform(alltask: taskModel[], task: string, taskParent: number, taskPriorityFrom: number, taskPriorityTo: number, startdate: Date, endDate: Date): taskModel[] {                
        if (alltask && alltask.length) {            
            return alltask.filter(item => {
                if (task && item.TaskName.toString().indexOf(task.toString()) === -1) {
                    return false;
                }                
                if (taskParent && item.Parent_ID.toString().indexOf(taskParent.toString()) === -1) {
                    return false;
                }
                if ((taskPriorityFrom && (item.Priority >= taskPriorityFrom) === false)) {
                    return false;
                }
                
                if ((taskPriorityTo && (item.Priority <= taskPriorityTo) === false)) {
                    return false;
                }                
                if (startdate && item.Start_Date.toLocaleString().indexOf(startdate.toLocaleString()) === -1) {
                    return false;
                }
                if (endDate && item.End_Date.toLocaleString().indexOf(endDate.toLocaleString()) === -1) {
                    return false;
                }
                return true;
            });
        }
        else {
            return alltask;
        }
    }
}
