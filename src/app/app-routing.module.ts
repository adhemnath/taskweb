import { NgModule } from '@angular/core';
import { ViewTaskComponent } from './view-task/view-task.component';
import { AddTaskComponent } from './add-task/add-task.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: ViewTaskComponent }, 
  { path: 'ViewTask', component: ViewTaskComponent }, 
  { path: 'AddTask', component : AddTaskComponent }, 
  { path: 'AddTask/:taskid', component : AddTaskComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
