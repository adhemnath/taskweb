import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common'
import { taskModel } from '../model/Task';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskServiceService } from 'src/app/task-service.service';

@Component({
  providers: [DatePipe],
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {

  constructor(private service: TaskServiceService, private router: Router, private route: ActivatedRoute, private datePipe: DatePipe) { }
  public task: taskModel;
  public taskId: number;
  public mode: string;

  ngOnInit() {
    this.task = new taskModel();
    this.taskId = parseInt(this.route.snapshot.paramMap.get("taskid"));

    var viewTask = this.setviewTask();
    var addTask = this.setaddTask();
    var btnAdd = this.getAddButton();

    this.mode = "Add";

    viewTask.className = "nav-link"
    addTask.className = "nav-link active";

    if (this.taskId) {
      this.mode = "Edit";
      viewTask.innerText = "";
      addTask.innerText = "Update Task";
      btnAdd.innerText = "Update"
      this.GetTaskbyId(this.taskId);
    }
  }

  GetTaskbyId(taskId: number) {
    var tsk: taskModel;
    this.service.getAllTasksByID(taskId).subscribe((TaskIDVar: taskModel) => {
      this.task = TaskIDVar;
      this.task.Start_Date = <any>this.datePipe.transform(this.task.Start_Date, "yyyy-MM-dd");
      this.task.End_Date = <any>this.datePipe.transform(this.task.End_Date, "yyyy-MM-dd");
    });
  }

  AddTask(task: taskModel) {
    //debugger;
    if (this.mode == "Add") {
      this.service.addTask(task).subscribe(result => { this.router.navigateByUrl('ViewTask') });
    }
    else {
      this.service.updateTask(task).subscribe(result => { this.router.navigateByUrl('ViewTask') });
    }

  }

  clearScreen() {
    //debugger;
    if (this.mode == "Edit") {
      this.router.navigateByUrl('ViewTask');
    }
    else {
      this.task = new taskModel();
    }
  }

  public setaddTask(): any {
    return document.getElementById("addTask");
  }
  public setviewTask(): any {
    return document.getElementById("viewTask");
  }

  public getAddButton(): any {
    return document.getElementById("btnAdd");
  }


}
