import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AddTaskComponent } from './add-task.component';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { TaskServiceService } from '../task-service.service';

describe('AddTaskComponent', () => {
  let component: AddTaskComponent;
  let fixture: ComponentFixture<AddTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, RouterTestingModule, HttpClientModule, BrowserModule],
      providers: [AddTaskComponent],      
      declarations: [ AddTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTaskComponent);
    component = fixture.componentInstance;

    let getValue: any = {};

    getValue.className = "nav-link";
    getValue.innerHTML = "Add Task";

    spyOn(component, 'setaddTask').and.returnValue(getValue);
    spyOn(component, 'setviewTask').and.returnValue(getValue);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('getTasksbyKeye', () => {
    component.GetTaskbyId(1007);
  });

});
