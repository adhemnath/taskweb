import { TestBed } from '@angular/core/testing';
import { TaskServiceService } from './task-service.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('TaskServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({
  imports: [HttpClientModule, HttpClientTestingModule],
  providers: [TaskServiceService]}));

  it('should be created', () => {
    const service: TaskServiceService = TestBed.get(TaskServiceService);
    expect(service).toBeTruthy();
  });
});
