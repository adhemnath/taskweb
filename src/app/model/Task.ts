export class taskModel{
    public Task_ID:number;
    public Parent_ID:number;
    public TaskName:string;
    public Priority:number;
    public Start_Date:Date;
    public End_Date:Date;
    public isTaskEnd:Boolean;
    //public Mode:string;
    //public Status:string;
    //public DisabledMode:boolean=false;

    constructor()
    {
        this.Priority=0;
    }
}
