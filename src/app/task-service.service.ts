import { Injectable } from '@angular/core';
import { HttpClient, HttpParams }  from '@angular/common/http';
import { AppConstants } from './constants';
import { taskModel } from './model/Task';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskServiceService {
  AppConstants: AppConstants = new AppConstants();
  constructor(private httpClient:HttpClient) { }

  getAllTasks():Observable<any>
  {
    //debugger;
    let url:string=this.AppConstants.baseUrl +this.AppConstants.getTasks;
    return this.httpClient.get(url);
  }

  getAllTasksByID(taskID:number):Observable<any>
  {
    let url:string=this.AppConstants.baseUrl +this.AppConstants.getTaskByID;
    let params=new HttpParams().set('id',taskID.toString());
    return this.httpClient.get(url,{params:params});
  }

  addTask(task:taskModel):Observable<any>
  {
    let url:string=this.AppConstants.baseUrl +this.AppConstants.addTask;     
    return this.httpClient.post(url,task);    
  }

  updateTask(task:taskModel):Observable<any>
  {
    let url:string=this.AppConstants.baseUrl +this.AppConstants.updateTask;     
    return this.httpClient.put(url,task);  
  }

  endTask(taskID:number):Observable<any>
  {        
    let url:string=this.AppConstants.baseUrl +this.AppConstants.endTask;
    let params=new HttpParams().set('id',taskID.toString());
    return this.httpClient.get(url,{params:params});  
  }

}
